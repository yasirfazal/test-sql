CREATE TABLE [dbo].[ADO NET Destination D1]
(
[Name] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Flat File Source.Value] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Data Conversion.Value] [int] NULL,
[isHobbit] [int] NOT NULL CONSTRAINT [DF__ADO NET D__isHob__267ABA7A] DEFAULT ((0))
) ON [PRIMARY]
GO
