CREATE TABLE [dbo].[ADO NET Destination S1]
(
[Name] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Flat File Source.Value] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Data Conversion.Value] [int] NULL,
[isHobbit] [int] NULL CONSTRAINT [DF__ADO NET D__isHob__286302EC] DEFAULT ((1))
) ON [PRIMARY]
GO
