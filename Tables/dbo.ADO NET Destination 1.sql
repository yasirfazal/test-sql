CREATE TABLE [dbo].[ADO NET Destination 1]
(
[Name] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Flat File Source.Value] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Data Conversion.Value] [int] NULL,
[isHobbit] [int] NULL
) ON [PRIMARY]
GO
