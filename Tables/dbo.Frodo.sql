CREATE TABLE [dbo].[Frodo]
(
[id] [int] NOT NULL IDENTITY(1, 1),
[Name] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Age] [int] NULL,
[isHobbit] [int] NULL,
[s1] [datetime] NULL,
[s2] [datetime] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
