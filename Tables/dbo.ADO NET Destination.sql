CREATE TABLE [dbo].[ADO NET Destination]
(
[Name] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Flat File Source.Value] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Data Conversion.Value] [int] NULL,
[isHobbit] [int] NULL,
[os] [int] NULL,
[tt] [nchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NewColumn] [int] NULL,
[NewColumn2] [int] NULL,
[New Col 3] [int] NULL,
[col1] [int] NULL,
[col2] [int] NULL,
[col3] [int] NULL,
[col4] [int] NULL,
[col32] [int] NULL,
[col321] [int] NULL,
[col1321] [int] NULL,
[col12321] [int] NULL
) ON [PRIMARY]
GO
